FROM nginx:alpine
LABEL project="FrontDevOps"
EXPOSE 80
COPY ./ /usr/share/nginx/html/